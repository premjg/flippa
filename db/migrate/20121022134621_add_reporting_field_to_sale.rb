class AddReportingFieldToSale < ActiveRecord::Migration
  def change
    add_column :sales, :report_month, :integer
    add_index :sales, :report_month
  end
end

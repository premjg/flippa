class CreateStockAlerts < ActiveRecord::Migration
  def change
    create_table :stock_alerts do |t|
      t.integer :product_id
      t.integer :average_2_wk_sale

      t.timestamps
    end
  end
end

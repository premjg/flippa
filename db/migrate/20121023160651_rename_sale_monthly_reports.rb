class RenameSaleMonthlyReports < ActiveRecord::Migration
  def up
    rename_table :sale_monthly_reports, :monthly_reports
  end

  def down
    rename_table :monthly_reports, :sale_monthly_reports
  end
end

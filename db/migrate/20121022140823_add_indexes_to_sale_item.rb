class AddIndexesToSaleItem < ActiveRecord::Migration
  def change
    add_index :sale_items, :sale_id
    add_index :sale_items, :product_id
  end
end

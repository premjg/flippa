class CreateSaleMonthlyReports < ActiveRecord::Migration
  def change
    create_table :sale_monthly_reports do |t|
      t.integer :month
      t.integer :sales
      t.integer :items
      t.decimal :revenue

      t.timestamps
    end
  end
end

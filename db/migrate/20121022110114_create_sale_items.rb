class CreateSaleItems < ActiveRecord::Migration
  def change
    create_table :sale_items do |t|
      t.integer :sale_id
      t.integer :product_id
      t.decimal :unit_price
      t.integer :quantity

      t.timestamps
    end
  end
end

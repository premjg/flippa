# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121024183241) do

  create_table "monthly_report_keepers", :force => true do |t|
    t.datetime "last_recorded_sale_at"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "monthly_reports", :force => true do |t|
    t.integer  "month"
    t.integer  "sales"
    t.integer  "items"
    t.decimal  "revenue",    :precision => 10, :scale => 0
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.integer  "quantity_in_stock", :null => false
  end

  create_table "sale_items", :force => true do |t|
    t.integer  "sale_id",                                  :null => false
    t.integer  "product_id",                               :null => false
    t.decimal  "unit_price", :precision => 5, :scale => 2, :null => false
    t.integer  "quantity",                                 :null => false
    t.datetime "created_at"
  end

  add_index "sale_items", ["created_at"], :name => "created_at"
  add_index "sale_items", ["product_id"], :name => "index_sale_items_on_product_id"
  add_index "sale_items", ["product_id"], :name => "sale_items_products_fkey"
  add_index "sale_items", ["sale_id"], :name => "index_sale_items_on_sale_id"
  add_index "sale_items", ["sale_id"], :name => "sale_items_sales_fkey"

  create_table "sales", :force => true do |t|
    t.datetime "created_at"
    t.integer  "report_month"
  end

  add_index "sales", ["created_at"], :name => "created_at"
  add_index "sales", ["report_month"], :name => "index_sales_on_report_month"

  create_table "stock_alerts", :force => true do |t|
    t.integer  "product_id"
    t.integer  "average_2_wk_sale"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

end

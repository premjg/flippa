Flippa::Application.routes.draw do
  resources :sale_items
  resources :products
  match "reports/monthly_report" => "reports#monthly_report", :as => "monthly_report"
  match "reports/detailed_report/:month" => "reports#detailed_report", :as => "detailed_report"
  root :to => "reports#monthly_report"
end

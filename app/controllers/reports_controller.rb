class ReportsController < ApplicationController
  helper_method :sort_column, :sort_direction

  def monthly_report
    @monthly_report = MonthlyReport.order(sort_column + " " + sort_direction)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @monthly_report }
    end
  end

  def detailed_report
    @month = params[:month]
    sort = sort_column + " " + sort_direction
    @detailed_report = Sale.detailed_report(@month, sort)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @detailed_report }
    end
  end

  private
  def sort_column
    %w(items revenue).include?(params[:sort]) ? params[:sort] : "items"
  end

  def sort_direction
    %w(asc desc).include?(params[:direction]) ? params[:direction] : "desc"
  end
end
class SaleItem < ActiveRecord::Base
  attr_accessible :product_id, :quantity, :sale_id, :unit_price
  belongs_to :product
  belongs_to :sale
end

class SaleItemObserver < ActiveRecord::Observer
  def after_create(sale_item)
    StockAlert.analyze(sale_item.product_id)
  end
end
class Product < ActiveRecord::Base
  attr_accessible :name, :quantity_in_stock
  has_many :sale_items
  has_many :stock_alerts
end

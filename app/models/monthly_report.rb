class MonthlyReport < ActiveRecord::Base
  attr_accessible :items, :month, :revenue, :sales
  # :month here is a unix time stamp for the beginning of the month
  def self.last_ran_at_unix
    last_run_row = limit(1).order('updated_at desc')
    last_run_row.present? ? last_run_row.first.updated_at.to_i : 0
  end

  # Find all sales that happened between the "last report run" timestamp's previous midnight to yesterday midnight
  # This ensures that sale transactions cannot 'slip' between reports
  # The latest data at any point is delayed by a maximum of ~24 hours
  # This is completely safe as it only updates months with partial data (Everything is inside a transaction block)
  # Or creates new months where none exists
  # Should be ideally called in a worker process
  # This is well covered with Unit Tests
  def self.populate_data
    last_run_previous_midnight = Time.at(self.last_ran_at_unix).yesterday.end_of_day # Last run timestamp's yesterday 23:59:59
    yesterday_midnight = Date.yesterday.end_of_day # Yesterday 23:59:59

    ActiveRecord::Base.transaction do
      Sale.joins(:sale_items)\
      .where("sales.created_at > ? AND sales.created_at <= ?", last_run_previous_midnight, yesterday_midnight)\
      .select("sales.report_month as month, count(DISTINCT sale_items.sale_id) as sales, count(sale_items.sale_id) as items, sum(sale_items.unit_price * sale_items.quantity) as revenue")\
      .group("sales.report_month")\
      .each do |sale|
        monthly_report = MonthlyReport.where(:month => sale.month).first
        if monthly_report.present? # Looks like this month has partial data. Append to the tally.
          monthly_report.sales += sale.sales
          monthly_report.items += sale.items
          monthly_report.revenue += sale.revenue
        else
          monthly_report = MonthlyReport.new({month: sale.month, sales: sale.sales, items: sale.items, revenue: sale.revenue})
        end
        monthly_report.save!
      end
    end
  end
end

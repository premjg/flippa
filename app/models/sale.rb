class Sale < ActiveRecord::Base
  after_create :set_report_month
  has_many :sale_items

  def set_report_month
    update_attribute(:report_month, start_of_month_unix_stamp)
  end

  def start_of_month_unix_stamp
    created_at.at_beginning_of_month().to_time.to_i
  end

  def self.detailed_report(month, sort="revenue desc")
    joins(:sale_items => :product)\
    .where("sales.report_month = ?", month)\
    .select("products.id, products.name as product_name, count(sale_items.sale_id) as items, sum(sale_items.unit_price * sale_items.quantity) as revenue")\
    .group("products.id")\
    .order(sort)
  end
end

class StockAlert < ActiveRecord::Base
  attr_accessible :average_2_wk_sale, :product_id
  belongs_to :product

  # Analyzes the last 8 weeks data (for good measure) and arrives at an average
  # If the current stock quantity is less than the average, an alert is created
  # At the time of sending the email - the product quantity is checked once again and the difference is what the seller has to place an order for.
  # This should ideally be delegated to a worker
  def self.analyze(product_id)
    average_2_wk_sale = (SaleItem.where("product_id = ? AND created_at >= ?", product_id, 8.weeks.ago).count / 4).ceil
    product = Product.find(product_id)
    if product.quantity_in_stock < average_2_wk_sale
      alert = find_or_create_by_product_id(product.id)
      alert.update_attribute(:average_2_wk_sale, average_2_wk_sale)
    end
  end

  # Send out the email and clear out the alerts
  def self.notify
    if StockAlert.all.count > 0
      StockAlertMailer.alert_email.deliver
      StockAlert.destroy_all
    end
  end
end

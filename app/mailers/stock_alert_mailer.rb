class StockAlertMailer < ActionMailer::Base
  default from: "alerter@giftshop.com"

  def alert_email()
    @stock_alerts = StockAlert.all
    mail(:to => ADMIN_EMAIL, :subject => "Stocke Alert Report as of #{Time.now.strftime("%d-%b-%Y %H:%M %z")}")
  end
end

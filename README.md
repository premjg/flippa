# Problem Description

An online gift shop records product sales in a database. The merchant requires
an aggregate view of the data, so that they can monitor overall sales
performance month-to-month.

A copy of the database has been provided in an SQL file.

## Requirements

**Part 1:**

Produce a small web application that can present the data in the following
ways:

  1. For each calendar month, list the total number of sales, the total number
     of items sold, and the total revenue raised from the sale of those items.
  2. When the user clicks on a given month in the above list, for each product,
     list the name of the product, the total number of items sold and the total
     revenue raised through sales of that item.
  3. Allow both of the above lists to be sorted by either total revenue, or
     total items sold.

It is not required that the report be calculated up to the current second. A
day or so behind is fine. Data never changes in the past.

**Part 2:**

Current stock levels are indicated in the products table. The merchant
requires **48 hours** to receive new stock when it is ordered from the
dealer.

Add a mechanism through which the merchant receives an alert by email if any
specific product is selling at such a rate that the merchant will run out of
stock **in the next two weeks**. Indicate the number of items that must be
ordered in order to keep the item in stock for two weeks.

### Expectations

  - We require the web application code, the stock level alert code and any
    test cases that back up the intended behaviour.
  - There are no time limits on the test, but please indicate approximately how
    long you spent on it.
  - You are not expected to apply any styling to the pages. Plain HTML tables
    without any CSS are fine.
  - You are free to add items to the schema, if you need to.
  - You may use any framework or ORM that you choose, but it must be in either
    Ruby or PHP.

**Good luck!**

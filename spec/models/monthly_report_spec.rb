require 'spec_helper'

describe MonthlyReport do

  it "returns the correct last_ran_at unix timestamp" do
    MonthlyReport.last_ran_at_unix.should eq(0)
    monthly_report = FactoryGirl.create(:monthly_report)
    MonthlyReport.last_ran_at_unix.should eq(monthly_report.updated_at.to_i)
  end

  describe "populate_data is called" do
    it "when unreported sales exist that are older than a day, it should populate at least one row" do
      sale_item1 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: 9.days.ago))
      sale_item2 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: 4.days.ago))
      sale_item3 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: 1.days.ago))
      MonthlyReport.populate_data()
      MonthlyReport.all.count.should be > 0
    end

    it "when unreported sales exist that are less than a day old, it should not populate any rows" do
      today_end_of_day = Date.today.end_of_day()
      sale_item1 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: (today_end_of_day - 23.hours)))
      sale_item2 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: (today_end_of_day - 4.minutes)))
      sale_item3 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: (today_end_of_day - 5.seconds)))
      MonthlyReport.populate_data()
      MonthlyReport.all.count.should eq(0)
    end
    
    it "when new sale records are found for an existing month, it correctly appends to the data for that month" do
      some_month = DateTime.new(2012,1,1,0,0,0).beginning_of_month.to_time
      some_month_unix = some_month.beginning_of_month.to_time.to_i
      monthly_report = FactoryGirl.create(:monthly_report, 
        :updated_at => (some_month + 25.seconds), # This is to ensure that the report runs from this time to yesterday
        :created_at => (some_month + 25.seconds),
        :month => some_month_unix, 
        :sales => 100, 
        :items => 130, 
        :revenue => 1300
      )
      sale_item1 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 10.days))
      sale_item2 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 200.minutes))
      sale_item3 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 30.seconds))
      
      sale_items = [sale_item1, sale_item2, sale_item3] 
      new_sales = new_items = sale_items.count # Because in the above tests each sale has only one item
      new_revenue = sale_items.inject(0) {|revenue, i| revenue + (i.quantity * i.unit_price)}

      MonthlyReport.populate_data()

      MonthlyReport.where(:month => some_month_unix).count.should eq(1)      
      
      updated_monthly_report = MonthlyReport.where(:month => some_month_unix)[0]
      updated_monthly_report.items.should eq(monthly_report.items + new_items) # Sales
      updated_monthly_report.sales.should eq(monthly_report.sales + new_sales) # Items
      updated_monthly_report.revenue.should eq(monthly_report.revenue + new_revenue.to_i) # Revenue
    end

    it "when new sale records are found for a new month, it correctly populates data for that month" do
      some_month = DateTime.new(2012,1,1,0,0,0).beginning_of_month.to_time
      some_month_unix = some_month.beginning_of_month.to_time.to_i
      sale_item1 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 10.days))
      sale_item2 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 200.minutes))
      sale_item3 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 30.seconds))
      
      sale_items = [sale_item1, sale_item2, sale_item3] 
      new_sales = new_items = sale_items.count # Because in the above tests each sale has only one item
      new_revenue = sale_items.inject(0) {|revenue, i| revenue + (i.quantity * i.unit_price)}

      MonthlyReport.populate_data()

      MonthlyReport.where(:month => some_month_unix).count.should eq(1) 
      
      monthly_report = MonthlyReport.where(:month => some_month_unix).first
      monthly_report.items.should eq(new_items) # Sales
      monthly_report.sales.should eq(new_sales) # Items
      monthly_report.revenue.should eq(new_revenue.to_i) # Revenue
    end
  end
end

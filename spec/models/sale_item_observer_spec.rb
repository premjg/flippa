require 'spec_helper'

describe SaleItemObserver do
  it "triggers a call to StockAlert when there's a sale", :current => true do
    product = FactoryGirl.create(:product_iron_man_mask)
    StockAlert.should_receive(:analyze).with(product.id)
    sale_item = FactoryGirl.create(:sale_item, :product => product)
  end
end

require 'spec_helper'

describe StockAlert do

  describe "When the stock's low" do
    before do
      StockAlert.stub(:analyze)
      @product = FactoryGirl.create(:product_iron_man_mask, :quantity_in_stock => 2)
      # selling two a week/avg
      [1.days.ago, 2.weeks.ago, 3.weeks.ago, 4.weeks.ago, 5.days.ago, 6.weeks.ago, 7.weeks.ago, 3.days.ago].each do |t|
        FactoryGirl.create(:sale_item, :product => @product, :created_at => t)
      end
    end
    it "creates an alert" do
      # Simulate a sale right now
      StockAlert.unstub(:analyze)
      @product.update_attribute(:quantity_in_stock, 1)
      FactoryGirl.create(:sale_item, :product => @product)
      StockAlert.where(:product_id => @product.id).count.should eq(1)
    end
  end
  describe "When there's enough stock" do
    before do
      StockAlert.stub(:analyze)
      @product = FactoryGirl.create(:product_iron_man_mask, :quantity_in_stock => 3)
      # selling one a week/avg
      [2.days.ago, 1.weeks.ago, 2.weeks.ago, 7.weeks.ago].each do |t|
        FactoryGirl.create(:sale_item, :product => @product, :created_at => t)
      end
    end
    it "do nothing" do
      # Simulate a sale right now
      StockAlert.unstub(:analyze)
      @product.update_attribute(:quantity_in_stock, 2)
      FactoryGirl.create(:sale_item, :product => @product)
      StockAlert.where(:product_id).count.should eq(0)
    end
  end

  describe "when notify is called", :current => true do
    before do
      StockAlertMailer.stub_chain(:alert_email, :deliver)
    end
    
    it "sends out an email when at least one alert is present" do
      FactoryGirl.create(:stock_alert)
      StockAlertMailer.should_receive(:alert_email)
      StockAlert.notify
    end
    it "does not send out an email when no alerts are present" do
      StockAlertMailer.should_not_receive(:alert_email)
      StockAlert.notify
    end
  end
end

require 'spec_helper'

describe Sale do
  
  it "returns the correct value for start_of_month_unix_stamp" do
    sale = FactoryGirl.create(:sale)
    sale.start_of_month_unix_stamp.should eq(sale.created_at.at_beginning_of_month().to_time.to_i)
  end

  describe "After Saving" do
    it "Calls the set_report_month method" do
      sale = FactoryGirl.build(:sale)
      sale.should_receive(:set_report_month)
      sale.save!
    end

    it "Has the correct report_month value" do
      sale = FactoryGirl.create(:sale)
      sale.report_month.should eq(sale.start_of_month_unix_stamp)
    end
  end

  it "returns correct data when detailed_report is called" do
    some_month = DateTime.new(2012,1,1,0,0,0).beginning_of_month.to_time
    some_month_unix = some_month.beginning_of_month.to_time.to_i

    sale_item1 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 10.days))
    sale_item2 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 200.minutes))
    sale_item3 = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 30.seconds))

    # Add a few other sales to try to throw off the report
    next_month_sale_item = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month + 40.days))
    prev_month_sale_item = FactoryGirl.create(:sale_item, :sale => FactoryGirl.create(:sale, created_at: some_month - 20.days))

    sale_items = [sale_item1, sale_item2, sale_item3]
    product_count = sale_items.collect(&:product_id).uniq.count
    revenue = sale_items.inject(0) {|rev, i| rev + (i.unit_price * i.quantity)}

    data = Sale.detailed_report(some_month_unix).all
    data.count.should eq(product_count)
    data.inject(0) {|rev, d| rev + d.revenue}.should eq(revenue)
  end
end

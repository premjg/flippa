require 'spec_helper'

describe "sale_items/index" do
  before(:each) do
    assign(:sale_items, [
      stub_model(SaleItem,
        :sale_id => 1,
        :product_id => 2,
        :unit_price => "9.99",
        :quantity => 3
      ),
      stub_model(SaleItem,
        :sale_id => 1,
        :product_id => 2,
        :unit_price => "9.99",
        :quantity => 3
      )
    ])
  end

  it "renders a list of sale_items" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end

require 'spec_helper'

describe "sale_items/new" do
  before(:each) do
    assign(:sale_item, stub_model(SaleItem,
      :sale_id => 1,
      :product_id => 1,
      :unit_price => "9.99",
      :quantity => 1
    ).as_new_record)
  end

  it "renders new sale_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sale_items_path, :method => "post" do
      assert_select "input#sale_item_sale_id", :name => "sale_item[sale_id]"
      assert_select "input#sale_item_product_id", :name => "sale_item[product_id]"
      assert_select "input#sale_item_unit_price", :name => "sale_item[unit_price]"
      assert_select "input#sale_item_quantity", :name => "sale_item[quantity]"
    end
  end
end

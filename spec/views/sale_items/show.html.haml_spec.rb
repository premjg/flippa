require 'spec_helper'

describe "sale_items/show" do
  before(:each) do
    @sale_item = assign(:sale_item, stub_model(SaleItem,
      :sale_id => 1,
      :product_id => 2,
      :unit_price => "9.99",
      :quantity => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/9.99/)
    rendered.should match(/3/)
  end
end

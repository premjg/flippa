require 'spec_helper'

describe "sale_items/edit" do
  before(:each) do
    @sale_item = assign(:sale_item, stub_model(SaleItem,
      :sale_id => 1,
      :product_id => 1,
      :unit_price => "9.99",
      :quantity => 1
    ))
  end

  it "renders the edit sale_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sale_items_path(@sale_item), :method => "post" do
      assert_select "input#sale_item_sale_id", :name => "sale_item[sale_id]"
      assert_select "input#sale_item_product_id", :name => "sale_item[product_id]"
      assert_select "input#sale_item_unit_price", :name => "sale_item[unit_price]"
      assert_select "input#sale_item_quantity", :name => "sale_item[quantity]"
    end
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sale_item do
    sale
    association :product, factory: [:product_capt_america_shield, :product_iron_man_mask].sample
    unit_price {[9.99, 10.99, 13.99, 9.10].sample}
    quantity {Array(1..10).sample}
  end
end

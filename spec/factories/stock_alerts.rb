# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stock_alert do
    product_id 1
    average_2_wk_sale 1
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :monthly_report do
    month 1
    sales 1
    items 1
    revenue 9.99
  end
end

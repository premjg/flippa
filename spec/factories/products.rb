# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    name "Some Product"
    quantity_in_stock {Array(1..10).sample}

    factory :product_iron_man_mask do
      name "Iron Man Mask"
    end

    factory :product_capt_america_shield do
      name "Captain America Shield"
    end

  end
end

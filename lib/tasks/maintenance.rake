task :update_monthly_report => :environment do
  puts "Running Monthly Report ...."
  MonthlyReport.populate_data()
  puts "Finsihed running Monthly Report"
end

task :send_stock_alerts => :environment do
  puts "Sending Stock Alerts ...."
  StockAlert.notify
  puts "Finsihed Sending Stock Alerts"
end